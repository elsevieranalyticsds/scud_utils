name := "scudUtils"

version := "1.0"

scalaVersion := "2.10.5"

libraryDependencies ++= Seq(
  "com.amazonaws" % "aws-java-sdk" % "1.7.4"
)

enablePlugins(SbtNativePackager)

enablePlugins(JavaAppPackaging)

enablePlugins(UniversalPlugin)
