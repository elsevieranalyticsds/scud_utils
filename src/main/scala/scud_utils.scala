import scala.collection.JavaConversions._
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.s3.model.{ListObjectsRequest, ObjectListing,
                                        GetObjectRequest}

package scud_utils {
  object fs {
    def ls(bucket: String, path: String):Array[String] ={
      var files = Array[String]()

      val s3Client = new AmazonS3Client(
                        new EnvironmentVariableCredentialsProvider())
      var listObjectsRequest = new ListObjectsRequest()
      var objectListing: ObjectListing = null
      listObjectsRequest.setBucketName(bucket)
      listObjectsRequest.setPrefix(path)
    
      do {
        objectListing = s3Client.listObjects(listObjectsRequest);
        objectListing.getObjectSummaries.map{o => 
          o.getKey.split("/").slice(0,path.split("/").size+1).mkString("/")
        }.foreach{ o =>
            val f = "s3a://" + bucket + "/" + o
            files = files :+ f
        }
        listObjectsRequest.setMarker(objectListing.getNextMarker)
      } while (objectListing.isTruncated)
      return files.distinct
    }
  }
}

